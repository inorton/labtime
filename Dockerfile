FROM alpine:latest

WORKDIR /
RUN apk add python3
ADD requirements.txt .
RUN python3 -m pip install -r requirements.txt
ADD LabTime /LabTime
ENV PYTHONPATH /
CMD python3 -m LabTime

EXPOSE 4040