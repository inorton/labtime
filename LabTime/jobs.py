"""
Wrappers for gitlabs Job result objects
"""
import dateutil.parser
from datetime import timezone, datetime


class SimpleJob(object):
    """
    Represent a gitlab job
    """
    def __init__(self, pipeline, stage, gitlabjob):
        self.pipeline = pipeline
        self.stage = stage
        self.job = gitlabjob
        self.cached = {}

    def if_status_return(self, status, value, default=""):
        """
        If the state is status, return value
        :param status:
        :param value:
        :param default: return this if status does not match
        :return:
        """
        if self.job.status == status:
            return value
        return default

    def __getitem__(self, item):
        if item in self.cached:
            return self.cached[item]
        value = None
        if item in ["created_at", "started_at", "finished_at"]:
            jobvalue = getattr(self.job, item)
            if jobvalue:
                value = dateutil.parser.parse(jobvalue)

            self.cached[item] = value
        return value

    def __getattr__(self, item):
        return self.__getitem__(item)

    def delayed(self):
        """
        If this job is complete, how log was it delayed/queued for
        :return:
        """
        if self.started():
            # time since creation of the pipeline and this job starting
            # if we are in a stage, use the stage delay time
            if self.stage:
                delay = (self.started() - self.stage.ready_at()).total_seconds()
            else:
                delay = (self.started() - self.created()).total_seconds()

            return delay
        if self.job.status == "pending":
            now = datetime.now(timezone.utc)
            return (now - self.stage.ready_at()).total_seconds()

        return 0

    def since_created(self):
        """
        Full time from creation to finish
        :return:
        """
        ended = self.finished_at()
        if not ended:
            ended = datetime.now(timezone.utc)

        if ended:
            return (ended - self.created()).total_seconds()
        return 0

    def created(self):
        """
        Get the creation timestamp of this job
        :return:
        """
        return self["created_at"]

    def started(self):
        """
        Get the start timestamp of this job
        :return:
        """
        return self["started_at"]

    def duration(self):
        """
        Return the duration of this job in seconds
        :return:
        """
        if not self.started():
            return 0

        if not self.job.duration:
            # still running
            now = datetime.now(timezone.utc)
            return (now - self.started()).total_seconds()

        return self.job.duration

    def finished_at(self):
        """
        Return the time the job finished
        :return:
        """
        return self["finished_at"]

    def name(self):
        """
        Get the name
        :return:
        """
        return self.job.name

    def __str__(self):
        return "Job #{} {}".format(self.job.id, self.name())