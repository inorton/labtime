"""
Handle authentication and settings
"""
import gitlab
import yaml
import dateutil.parser
from datetime import datetime, timezone
from cachetools.func import ttl_cache
from .jobs import SimpleJob


class SimplePipelineList(list):
    """
    A List of pipelines
    """
    def __init__(self, content=None):
        super(SimplePipelineList, self).__init__()
        if content:
            self.extend(content)

    def slowest(self):
        """
        Get the slowest pipeline in the list
        :return:
        """
        slow = self[0]
        for pipe in self:
            if pipe.since_created() > slow.since_created():
                slow = pipe

        return slow


class SimplePipeline(object):
    """
    A single pipeline
    """
    def __init__(self, pipelines, pipeline):
        self.list = pipelines
        self.pipeline = pipeline
        self.id = pipeline.id
        self.stages = []

    def __str__(self):
        return "Pipeline: #{}".format(self.id)

    def minutes(self):
        """
        How long was the pipeline in whole minutes
        :return:
        """
        seconds = self.duration()
        if seconds:
            return int(seconds / 60)
        return 0

    def created_at(self):
        """
        When the pipeline was created
        :return:
        """
        return dateutil.parser.parse(self.pipeline.created_at)

    def started_at(self):
        """
        When the pipeline was started
        :return:
        """
        if self.pipeline.started_at:
            return dateutil.parser.parse(self.pipeline.started_at)
        return None

    def finished_at(self):
        """
        When the pipeline ended (or now if it is still running)
        :return:
        """
        if self.pipeline.finished_at:
            return dateutil.parser.parse(self.pipeline.finished_at)
        return datetime.now(timezone.utc)

    def delayed(self):
        """
        How long did this pipeline wait to start
        :return:
        """
        delay_time = 0
        finished = self.finished_at()
        if finished:
            delay_time = self.since_created() - self.since_started()

        return delay_time

    def since_created(self):
        """
        Time passed since this pipeline was created and finished (or now if still running)
        :return:
        """
        end = self.finished_at()
        if not end:
            end = datetime.now(timezone.utc)
        span = (end - self.created_at()).total_seconds()
        return span

    def since_started(self):
        """
        Time passed since this pipeline was started and finished (or now if still running)
        :return:
        """
        if self.started_at():
            end = self.finished_at()
            if not end:
                end = datetime.now(timezone.utc)
            span = (end - self.started_at()).total_seconds()
            return span
        return 0

    def duration(self):
        """
        How long gitlab reports pipeline took in seconds excluding being queued/delayed.

        This seems to bear no relation to reality
        :return:
        """
        if self.pipeline.duration:
            return self.pipeline.duration

        # running
        return 0

    def best_possible_duration(self):
        """
        Get the critical path duration of this pipeline
        :return:
        """
        seconds = 0
        if self.pipeline.duration:
            for stage in self.stages:
                length = 0
                for job in stage.jobs:
                    jobduration = job.duration()
                    if jobduration and jobduration > length:
                        length = jobduration
                seconds += length

        # fudge in 45 sec per stage leeway

        return seconds + (45 * len(self.stages))

    def queue_overhead(self):
        """
        Return the number of seconds all stages were blocked for
        :return:
        """
        whole = self.since_created()
        best = self.best_possible_duration()
        if best:
            return whole - best
        return 0


class SimpleStage(object):
    """
    Stages in a job
    """
    def __init__(self, pipeline, name, previous=None):
        self.pipeline = pipeline
        self.name = name
        self.jobs = []
        self.previous = previous

    def __str__(self):
        return self.name

    def __repr__(self):
        return str(self)

    def add_job(self, job):
        self.jobs.append(job)

    def last_job(self):
        """
        Get the last job to finish
        :return:
        """
        if self.jobs:
            last = self.jobs[0]
            for job in self.jobs:
                if job.finished_at() and last.finished_at():
                    if job.finished_at() > last.finished_at():
                        last = job
            return last
        return None

    def duration(self):
        """
        The time taken for all the jobs in this stage took to execute from first to start to
        last to finish
        :return:
        """
        first = self.first_job()
        if first:
            last = self.last_job()
            if last:
                return (last.finished_at() - first.started()).total_seconds()
        return 0

    def started_at(self):
        """
        Time the first job in this stage started
        :return:
        """
        first = self.first_job()
        if first:
            return first.started()
        return None

    def first_job(self):
        """
        Find the first job that started in this staged
        :return:
        """
        if self.jobs:
            start = self.jobs[0]
            for job in self.jobs:
                if job.started() < start.started():
                    start = job
            return start
        return None

    def delayed(self):
        """
        Return how many seconds this stage was delayed by overall (how long the last job to finish was delayed for)
        :return:
        """
        ready = self.ready_at()
        start = self.started_at()
        if start and ready:
            return (start - ready).total_seconds()

        return 0

    def ready_at(self):
        """
        Get the time where this stage could have first started
        :return:
        """
        if self.previous:
            last_job = self.previous.last_job()
            last_fin = last_job.finished_at()
            return last_fin
        return self.pipeline.created_at()

    def since_ready(self):
        """
        Get the time between the stage being startable to it finishing
        :return:
        """
        ready = self.ready_at()
        running = False
        for job in self.jobs:
            if job.job.status == "running":
                running = True
                break
        finish = self.last_job().finished_at()
        if running or not finish:
            finish = datetime.now(timezone.utc)

        if not ready:
            ready = self.pipeline.created_at()

        return (finish - ready).total_seconds()

    def __str__(self):
        return "Stage: {}".format(self.name)


class GitlabSession(object):
    """
    Wrap up token based authentication and loading configuration from disk
    """
    def __init__(self):
        self.server = None
        self.token = None
        self.projects = []
        self.cfgfile = None
        self.gitlab = None

    def load(self, cfgfile):
        """
        Load our configuration from disk
        :param cfgfile:
        :return:
        """
        self.cfgfile = cfgfile
        self.gitlab = None
        self.reload()

    def reload(self):
        """
        Load configuration from disk
        :return:
        """
        with open(self.cfgfile, "r") as cfg:
            settings = yaml.load(cfg, yaml.SafeLoader)
        self.server = settings.get("server")
        self.projects = settings.get("projects", [])
        self.token = settings.get("token")
        self.gitlab = gitlab.Gitlab(self.server, private_token=self.token)

    @ttl_cache(maxsize=50, ttl=60)
    def get_project(self, project_id):
        """
        Get a project
        :param project_id:
        :return:
        """
        return self.gitlab.projects.get(project_id)

    @ttl_cache(maxsize=500, ttl=45)
    def get_pipelines(self, project_id, limit=10, page=1):
        """
        Get the most recent pipelines
        :param project_id:
        :param limit:
        :return:
        """
        if limit > 100:
            limit = 100

        project = self.get_project(project_id)
        if not project:
            return None
        pipes = SimplePipelineList()
        for pipe in project.pipelines.list(page=page, per_page=limit):
            pipeline = self.get_pipeline(project_id, pipe.id)
            if pipeline:
                pipeline.list = pipes
                pipes.append(pipeline)

        return pipes

    def get_pipeline(self, project_id, id):
        """
        Get a pipeline and all it's stages and jobs
        :param project_id:
        :param id:
        :return:
        """
        project = self.get_project(project_id)
        pipeline = project.pipelines.get(id)

        simple = SimplePipeline(None, pipeline)
        simple.stages = self.get_jobs(pipeline)
        return simple

    @staticmethod
    @ttl_cache(maxsize=1000, ttl=300)
    def get_jobs(pipeline):
        """
        Get simple jobs from a pipeline
        :param pipeline:
        :return:
        """
        jobs = pipeline.jobs.list(per_page=100)
        stages = []
        stage = None
        stage_dict = {}
        simple_pipeline = SimplePipeline(None, pipeline)

        previous_stage = None
        for job in jobs:
            stage = stage_dict.get(job.stage)

            if not stage:  # new stage?
                stage = SimpleStage(simple_pipeline, job.stage, previous=previous_stage)
                stages.append(stage)
                stage_dict[job.stage] = stage
                previous_stage = stage

            stage = stage_dict[job.stage]
            stage.add_job(SimpleJob(pipeline, stage, job))

        return stages
