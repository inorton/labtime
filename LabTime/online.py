import os
import time
import logging
from .session import GitlabSession, SimplePipelineList
from flask import Flask, render_template, send_from_directory, request
from threading import Thread

app = Flask(__name__)


s = GitlabSession()
s.load("config.yml")
projects = []
for project_id in s.projects:
    projects.append(s.get_project(project_id))


def poll_projects():
    """
    Keep the cache warm
    :return:
    """
    while True:
        time.sleep(30)
        logging.info("Polling for projects..")
        for p in projects:
            s.get_project(p.id)
            s.get_pipelines(p.id, limit=20)


poll_thread = Thread(target=poll_projects, daemon=True)
poll_thread.start()


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory(os.path.join('static', 'js'), path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory(os.path.join('static', 'css'), path)


@app.route('/fonts/<path:path>')
def send_fonts(path):
    return send_from_directory(os.path.join('static', 'fonts'), path)


@app.route("/")
def index():
    return render_template("index.html", projects=projects)


@app.route('/project/<int:project_id>')
def project(project_id):
    page = int(request.args.get("page", 1))
    if page < 1:
        page = 1
    ref = request.args.get("ref", None)
    refs = []

    delayed = []

    proj = s.get_project(project_id)
    pipelines = s.get_pipelines(project_id, limit=20, page=page)

    for pipe in pipelines:
        reference = pipe.pipeline.ref
        if reference not in refs:
            refs.append(reference)

    if ref is not None:
        pipelines = SimplePipelineList([p for p in pipelines if p.pipeline.ref == ref])

    delayed_jobs = {}
    for pipe in pipelines:
        for stage in pipe.stages:
            for job in stage.jobs:
                name = job.name()
                if name in delayed_jobs:
                    delayed_jobs[name] += job.delayed()
                else:
                    delayed_jobs[name] = job.delayed()

    delayed = sorted(delayed_jobs.items(), key=lambda x: x[1], reverse=True)[:10]

    longest_duration = 0
    slowest = pipelines.slowest()
    if slowest:
        longest_duration = slowest.since_created()

    return render_template("project.html",
                           project=proj,
                           longest_duration=longest_duration,
                           page=page,
                           pipelines=pipelines,
                           delayed=delayed,
                           refs=refs,
                           )


@app.route('/pipeline/<int:project_id>/<int:pipeline_id>')
def pipeline(project_id, pipeline_id):
    proj = s.get_project(project_id)
    pipeline = s.get_pipeline(project_id, pipeline_id)

    return render_template("pipeline.html",
                           project=proj,
                           pipeline=pipeline)
