"""
Functions for sorting results
"""

def jobs_by_start(jobs):
    """
    Return the jobs sorted in order of start time
    :param jobs:
    :return:
    """
    return sorted([x for x in jobs if x.started_at], key=lambda x: x.started_at)
