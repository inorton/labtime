#!/usr/bin/python3
from __future__ import print_function
from LabTime import session

s = session.GitlabSession()
s.load("config.yml")

for project_id in s.projects:
    project = s.get_project(project_id)
    print(project.name)
    pipes = s.get_pipelines(project_id, limit=10)
    for pipe in pipes:
        print("-" * 60)
        print("= {} =".format(pipe.id))
        stages = s.get_jobs(pipe)
        for stage in stages:
            print("Stage {}".format(stage.name))
            for job in stage.jobs:
                duration = job.duration()
                if not duration:
                    print("{} was not run".format(job.name()))
                else:
                    print("{} took {}s".format(job.name(), duration))
